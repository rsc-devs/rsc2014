<?php if ($content): ?>
  <section class="<?php print $classes; ?>">
    <h1 class="element-invisible">Left sidebar</h1>
    <?php print $content; ?>
  </section>
<?php endif; ?>
