<?php if ($content): ?>
  <section class="<?php print $classes; ?>">
    <h1 class="element-invisible">Right sidebar</h1>
    <?php print $content; ?>
  </section>
<?php endif; ?>
