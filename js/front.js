/**
 * @file
 * Javascript which loads on the front page.
 */
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.rsc2014_front = {
  attach: function(context, settings) {

    const slider = $('#block-views-rsc-slider-view-block');
    const parent = slider.parent();

    var respond = function(){
      // Remove the "display: none" set by css, to display these blocks only on
      // browsers with JS, where the container is wide enough:
      if (parent.width() > 520) {
        slider.slideDown();
      }
      else {
        slider.slideUp();
      }
    };

    $(window).load(respond);   // after document ready
    $(window).resize(respond); // on window resize
    
  }
};


})(jQuery, Drupal, this, this.document);
