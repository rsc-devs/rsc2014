/**
 * @file
 * Javascript which loads on all pages
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal) {

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.rsc2014_all = {
    attach: function (context, settings) {
      // Stop addtoany from misbehaving and messing up vertical rhythm.
      $('.a2a_kit_size_32').removeClass();

      /**
       * Respond to the resizing of the browser window.
       */
      function respond() {
        // Fit the audio element between the headphones icon and the download
        // link when the div is wide enough.
        $(".rsc-attachment-formatter-audio").each(function (index, element) {
          const e = $(element);
          const audio = e.find('audio');
          const link = e.find('a');
          const icon_width = 50;
          const threshold = 500;

          if (e.width() > threshold) {
            audio.width(e.width() - icon_width - link.outerWidth());
            e.css('background-position', 'left center');
          }
          else {
            audio.width('100%');
            e.css('background-position', 'left bottom');
          }
        });

        const line_height = 22; // hard not to hardcode

        // Make the height of sidebar images a multiple of the line height by
        // cropping it slightly using the surrounding a tag's height property.
        const a = $(".block-rsc-library article").find("span.field-name-rscl-featured-image").find("a");
        $.each(a, function () {
          const img = $(this).find("img").first();
          const lines = (img.height() / line_height >> 0); // integer division
          $(this).height(lines * line_height);
        });

        // Preserve vertical rhythm for video elements by modifying the
        // surrounding div's height property.
        const videos = $(".rsc-attachment-formatter-video > video");
        $.each(videos, function () {
          const video = $(this);
          const div = video.parent();
          const lines = (video.height() / line_height >> 0); // integer division
          // +1 line to add space instead of cropping.
          // +2 lines for the download button.
          div.height((lines + 3) * line_height);
        });
      }

      $(window).load(respond);   // after document ready
      $(window).resize(respond); // on window resize
    }
  };

})(jQuery, Drupal);
