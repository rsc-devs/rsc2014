/**
 * @file
 * Javascript which loads on RSC GiG pages
 */
(function ($, Drupal) {
  Drupal.behaviors.rsc2014_gig = {
    attach: function (context, settings) {
      /**
       * Respond to the resizing of the browser window.
       */
      function respond() {
        const signatures = $("article.node-rsc-gig-certificate .signatures");
        const stamp_div = $("article.node-rsc-gig-certificate .stamp");
        const stamp_img = $("article.node-rsc-gig-certificate .stamp img");
        if (signatures.width() > 500) {
          signatures.css("grid-auto-flow", "column");
          stamp_div.css("height", "66px"); // 66px==rhythm(3)
          stamp_img.css("position", "relative");
        }
        else {
          signatures.css("grid-auto-flow", "row");
          stamp_div.css("height", "auto");
          stamp_img.css("position", "static");
        }
      }

      $(window).load(respond);   // after document ready
      $(window).resize(respond); // on window resize
    }
  };
})(jQuery, Drupal);
